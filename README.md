# scriptsCollection

## What is this repository about?

This repository only contains categorized, generic and DBA-scoped script hacks. 

The idea of this repository is not to be fancy, is to be useful. This is intended
to be your base ground for using scripts to gather quick information for issues by topic.

Imagine building scripts for the following questions:

Why CPU is so high?
Why Load Average is high?
What is Postgres doing at IO level?
Is it NUMA enabled? Are nodes balanced?
How many cores are busy at the moment?

So, those scripts should be here.


## Organization and formats

- use `.<script_language>` for scripts.
- use `.md` within the same file name as the script file extension if you want to include documentation of the query and its contents.

NOTE:

> Please add documentation as you were an alien and you need  to understand what the 
> script is for and how execute it.


## Practices 

Rule 1:

> Don't be fancy. **Make it work**


## Testing

TODO

(We do have an easy way to spin up docker images from all available versions and trigger queries
against them). Probably we can reuse postgresqlco.nf infrastructure for this.


## Searching

### Le ol' `git grep`

Use `git grep <pattern>` for searching in th repo.


