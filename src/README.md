## Categories

Example categories:

- Memory
- OOM
- CPU
- Tracing
- Swap
- Disk
  - FS
  - IO
  - Corruption
  
... etc.

Scripts should be customer-agnostic and as much resusable as possible. The idea is not
to be fancy here, is to make it work.


